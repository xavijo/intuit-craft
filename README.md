Steps to use this package

Clone and setup this package in your machine
cd into the cloned folder and run 'npm install'
This will install all the necessary packages that's needed to run this website
To start this in development mode, run 'npm run start'
To start this in production mode, firslty
a. Run 'npm run build', to create the assets
b. Install a static server like 'serve' using 'npm install -g serve' and run 'serve -s build'