import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Autosuggest from 'react-autosuggest';
import debounce from 'lodash/debounce';

import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';
import CloseIcon from '@material-ui/icons/Close';

import { replaceAll } from '../../../utils'

import {
	renderSuggestion,
	renderInputComponent,
	shouldRenderSuggestions
} from './search-utils';

import { getAutoSuggestion } from '../../../service/ajax-client';

const styles = theme => ({
	root: {
		flexGrow: 1,
		position: 'relative'
	},
	suggestionsContainerOpen: {
		position: 'absolute',
		zIndex: 1,
		marginTop: theme.spacing(1),
		left: 0,
		right: 0,
		boxShadow: '0 3px 6px rgba(0, 0, 0, 0.15)'
	},
	suggestion: {
		display: 'block'
	},
	suggestionsList: {
		margin: 0,
		padding: 0,
		listStyleType: 'none'
	},
	divider: {
		height: theme.spacing(2)
	},
	clearIcon: {
		color: '#aaaaaa',
		position: 'absolute',
		right: '2%',
		top: 0,
		bottom: 0,
		margin: 'auto',
		cursor: 'pointer'
	}
});


class SearchBar extends React.Component {
    constructor() {
        super();
        this.state = {
            value: '',
            suggestions: [],
            isLoading: false,
            error: null,
            isFocused: false,
        };

        this.debouncedLoadSuggestions = debounce(this.loadSuggestions, 500);
    }

    onSuggestionsClearRequested = () => {
		this.setState({
			suggestions: []
		});
	};

    onSuggestionSelected = (
		event,
		// eslint-disable-next-line no-unused-vars
		{ suggestion, suggestionValue, suggestionIndex, sectionIndex, method }
	) => {
		event.stopPropagation();
		event.target.blur();

        this.setState({ isFocused: false });

        const redirectURL = `/search?query="${encodeURIComponent(
            suggestion['1. symbol']
        )}"`;

		this.props.history.push(redirectURL);
	};

	getSuggestionValue = suggestion => {
		return suggestion['1. symbol'];
	};

    loadSuggestions = ({ value }) => {
		const self = this;
		this.setState({
			isLoading: true
		});

		const payload = {
			phrase: value,
		};

		getAutoSuggestion(payload)
			.then(response => {
				self.setState({
					suggestions: response.bestMatches || [],
					isLoading: false
				});
			})
			.catch(error => {
				self.setState({
					isLoading: false,
					suggestions: [],
					error: error
				});
			});
	};

    onSubmit = e => {
		if (e.key === 'Enter') {
			const { value } = this.state;

			this.setState({ isFocused: false });

			const redirectURL = `/search?query="${encodeURIComponent(
				value
			)}"`;

			this.props.history.push(value ? redirectURL : '/search');
		}
	};

    onChange = (_event, { newValue }) => {
		let isFocused = false;

		//in all other cases, the value should still be updated.
		this.setState({
			value: newValue,
			isFocused
		});
	};

    handleFocus = () => {
		this.setState({ isFocused: true });
	};

	handleBlur = e => {
		this.setState({ isFocused: false });
	};

    clearInput = () => {
		this.setState({
			value: ''
		});
	};

    componentDidMount() {
		const { search } = this.props.location;
		const queryParams = new URLSearchParams(search);
		const value = decodeURIComponent(
			replaceAll(queryParams.get('query') || '', '"', '')
		);
		this.setState({ value });
	}

    render() {
		const { value, suggestions, isFocused } = this.state;
		const { classes } = this.props;


		const autosuggestProps = {
			renderInputComponent,
			suggestions,
            onSuggestionsFetchRequested: this.debouncedLoadSuggestions,
			onSuggestionsClearRequested: () => this.onSuggestionsClearRequested(),
			onSuggestionSelected: this.onSuggestionSelected,
			getSuggestionValue: this.getSuggestionValue,
			renderSuggestion,
			focusInputOnSuggestionClick: false,
			shouldRenderSuggestions
		};
		return (
			<div className={classes.root}>
				<Paper
					square
					style={{
						width: `${this.props.width}`,
						margin: `${this.props.margin}`
					}}
					className="card-shadow"
				>
					<Autosuggest
						{...autosuggestProps}
						inputProps={{
							classes,
							placeholder: 'Search',
							value: value,
							onChange: this.onChange,
							onFocus: this.handleFocus,
							onBlur: this.handleBlur,
							onKeyPress: this.onSubmit
						}}
						theme={{
							container: this.props.styling,
							suggestionsContainerOpen: classes.suggestionsContainerOpen,
							suggestionsList: classes.suggestionsList,
							suggestion: classes.suggestion
						}}
						renderSuggestionsContainer={options => {
							return (
								!isFocused && (
									<Paper {...options.containerProps} square>
										{options.children}
									</Paper>
								)
							);
						}}
					/>
					{value && (
						<>
							<CloseIcon
								onClick={this.clearInput}
								className={classes.clearIcon}
							/>
						</>
					)}
				</Paper>
			</div>
		);
	}
}

SearchBar.propTypes = {
	classes: PropTypes.object.isRequired,
	width: PropTypes.string,
	styling: PropTypes.object.isRequired,
	margin: PropTypes.string.isRequired,
	history: PropTypes.object.isRequired,
	location: PropTypes.object.isRequired,
};

export default withStyles(styles)(withRouter(SearchBar));