import React from 'react';

import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

export const renderInputComponent = inputProps => {
    const { classes, inputRef = () => {}, ref, ...other } = inputProps;
  
    return (
      <TextField
        fullWidth
        InputProps={{
          inputRef: node => {
            ref(node);
            inputRef(node);
          },
          classes: {
            input: classes.input
          },
          disableUnderline: true
        }}
        {...other}
      />
    );
};
  
export const renderSuggestion = (suggestion, { query, isHighlighted }) => {
    return (
        <MenuItem
            selected={isHighlighted}
            component="div"
            classes={{
                selected: 'search-suggestion-selected'
            }}
        >
        <div style={{ width: '100%'}}>
            <div style={{ display: 'inline-block' }}>
                {suggestion['1. symbol']}
            </div>
            <div style={{ float: 'right', display: 'inline-block' }}>
                {suggestion['2. name']}
            </div>
        </div>
        </MenuItem>
    );
};

export const shouldRenderSuggestions = value => {
    return value.trim().length > 1;
};