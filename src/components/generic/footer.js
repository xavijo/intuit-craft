import React from 'react';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';

const Footer = ({ position = 'relative' }) => {
	return (
		<div className="footer" style={{ position }}>
			<Grid
				container
				spacing={0}
				justifyContent="space-between"
				alignItems="center"
				alignContent="center"
			></Grid>
		</div>
	);
};

Footer.propTypes = {
	position: PropTypes.string
};

export default Footer;
