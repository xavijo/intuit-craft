import React from 'react';
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from '@material-ui/core/CircularProgress';
import { replaceAll } from '../../utils'
import { getDetailsForSymbol } from '../../service/ajax-client';
import { withRouter } from 'react-router'

const styles = () => ({});

class SearchBase extends React.Component {
    state = {
        overview: {},
        query: '',
        isLoading: false
    }

    getQueryFromUrl = (search) => {
        const queryParams = new URLSearchParams(search);
        const phrase = decodeURIComponent(
            replaceAll(queryParams.get('query') || '', '"', '')
        );

        return phrase;
    }

    componentDidMount() {
        const self = this;
        const { location: { search } = {} } = this.props;
        const phrase = this.getQueryFromUrl(search);

        if (phrase !== '') {
            this.setState({
                isLoading: true
            });
            getDetailsForSymbol({ phrase }).then((response) => {
                self.setState({
                    query: phrase,
                    overview: response,
                    errored: false,
                    isLoading: false
                });
            }).catch((err) => {
                self.setState({
                    query: phrase,
                    overview: {},
                    errored: true,
                    isLoading: false
                });
            });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const self = this;
        const { location: { search: currentSearch } = {} } = this.props;
        const { location: { search: prevSearch } = {} } = prevProps;
        const currentPhrase = this.getQueryFromUrl(currentSearch);
        const prevPhrase = this.getQueryFromUrl(prevSearch);

        if (currentPhrase !== prevPhrase) {
            if (currentPhrase !== '') {
                this.setState({
                    isLoading: true
                });
                getDetailsForSymbol({ phrase: currentPhrase }).then((response) => {
                    if (Object.keys(response).length === 0) {
                        self.setState({
                            query: currentPhrase,
                            overview: {},
                            errored: true,
                            isLoading: false
                        });
                    } else {
                        self.setState({
                            query: currentPhrase,
                            overview: response,
                            errored: false,
                            isLoading: false
                        });
                    }
                }).catch((err) => {
                    console.log('err', err);
                    self.setState({
                        query: currentPhrase,
                        overview: {},
                        errored: true,
                        isLoading: false
                    });
                });
            } else {
                self.setState({
                    query: currentPhrase,
                    overview: {},
                    errored: false
                })
            }
        }
    } 

    render() {
        let { overview = {}, errored = false, query = '', isLoading } = this.state;
        const {Symbol, Name, Description, Exchange, Industry, PERatio, MarketCapitalization, Currency} = overview;

        if (Object.keys(overview).length === 0) {
            errored = true;
        }

        return (
            <div>
                { isLoading ? <div><CircularProgress /></div> :
                <>{ errored ? 
                    <div>{query !== '' ? <p>The symbol searched for does not exist or you have exeeced the limit of 5 api calls per minute, check after a couple of mins</p> : <p>Search and find the desired stock information</p>}</div> : 
                    <div>
                        <p>{`Symbol: ${Symbol}`}</p>
                        <p>{`Name: ${Name}`}</p>
                        <p>{`Description: ${Description}`}</p>
                        <p>{`Exchange: ${Exchange}`}</p>
                        <p>{`Industry: ${Industry}`}</p>
                        <p>{`Market Capitalization: ${MarketCapitalization}`}</p>
                        <p>{`Currency: ${Currency}`}</p>
                        <p>{`PE Ratio: ${PERatio}`}</p>
                    </div>
                }</>}
            </div>
        );
    }
}

export default withStyles(styles)(withRouter(SearchBase));