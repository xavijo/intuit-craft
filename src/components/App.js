import './App.css';
import React from 'react';
import AppDrawer from './drawer/app-drawer';
import Footer from './generic/footer';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import * as RouteLinks from '../constants/route-links';
import SearchBase from './search/search-base';

import '../styles/home.scss';

class App extends React.Component {
	render() {
		return (
			<div className="App">
				<BrowserRouter>
					<Switch>
						<Route path="/">
							{' '}
							<AppDrawer>
								<Switch>
									<Route exact path={RouteLinks.SEARCH_URL} component={SearchBase} />
								</Switch>
							</AppDrawer>
							<Footer />
						</Route>
					</Switch>
				</BrowserRouter>
			</div>
		);
	}
}

export default App;
