import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import SearchBar from '../generic/search-bar';

const styles = theme => ({
	root: {
		display: 'flex',
		minHeight: 'calc(100vh - 50px)',
		width: '100%'
	},
	appBar: {
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		}),
		boxShadow: '0 3px 6px rgba(0, 0, 0, 0.16)'
	},
	drawerHeader: {
		display: 'flex',
		alignItems: 'center',
		padding: theme.spacing(0, 1),
		// necessary for content to be below app bar
		...theme.mixins.toolbar,
		justifyContent: 'flex-end'
	},
	drawerHeaderColor: {
		background: '#3F51B5',
		height: '64px',
		boxShadow: '0 3px 6px rgba(0, 0, 0, 0.16)',
		zIndex: 1
	},
	content: {
		flexGrow: 1,
		padding: `0 ${theme.spacing(3)}px`,
	}
});

class AppDrawer extends React.Component {
	render() {
		const { classes } = this.props;

		return (
			<div className={classes.root}>
				<CssBaseline />
				<AppBar
					position="fixed"
					className={classes.appBar}
				>
					<Toolbar>
						<Typography variant="h6" className={classes.title} style={{ width: '200px' }}>
							Stock Details
						</Typography>
						<Grid container alignContent="center" justifyContent="center" spacing={0}>
							<Grid item style={{ width: '540px' }}>
								<SearchBar
									styling={{
										position: 'relative',
										padding: '0.5rem 1rem'
									}}
									margin="0"
								/>
							</Grid>
						</Grid>
					</Toolbar>
				</AppBar>
				<main className={classes.content}>
					<div className={classes.drawerHeader} />
					{this.props.children}
				</main>
			</div>
		);
	}
}

export default withStyles(styles, { withTheme: true })(AppDrawer);

AppDrawer.propTypes = {
	classes: PropTypes.object.isRequired,
	theme: PropTypes.object.isRequired,
	children: PropTypes.element.isRequired
};
