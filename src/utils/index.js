export const replaceAll = (str, find_text, replace_text) => {
    return str.split(find_text).join(replace_text);
};