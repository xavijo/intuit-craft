import axios from 'axios';

const BASE_URL = 'https://www.alphavantage.co';

const client = axios.create({
	baseURL: BASE_URL,
});

const axiosWrapper = options => {
	const onSuccess = response => {
		return response.data;
	};
	const onError = error => {
		return Promise.reject(error.response || error.message);
	};

	return client(options).then(onSuccess).catch(onError);
};

export default axiosWrapper;