import axiosWrapper from './axios-wrapper';
const API_KEY = '&apikey=TZH6UNXETVV73BH1'

const HTTP_METHOD = {
	GET: 'GET',
	POST: 'POST',
	PUT: 'PUT',
	DELETE: 'DELETE'
};

const FUNCTION_TYPES = {
    suggestion_search: 'SYMBOL_SEARCH',
    overview: 'OVERVIEW'
};

function checkResponse(result = {}) {
    if (result && (Object.keys(result).length === 0 || (result['note'] && result['note'].includes('Our standard API call frequency is 5 calls per minute')) || (result['bestMatches'] && result['bestMatches'].length === 0))) {
        return false;
    } else {
        return true;
    }
}

export const getAutoSuggestion = async ({ phrase }) => {
    let suggestion_api_cache = {};
    try {
        suggestion_api_cache = JSON.parse(localStorage.getItem('SUGGESTION_API_CACHE')) || {};
    } catch(e) {
        console.log(e);
    }

    if (!suggestion_api_cache[phrase]) {
        const result =  await axiosWrapper({
            url: `query?function=${FUNCTION_TYPES.suggestion_search}&keywords=${phrase}${API_KEY}`,
            method: HTTP_METHOD.GET
        });

        let isValid = checkResponse(result);

        if (isValid) { 
            suggestion_api_cache[phrase] = result;
            localStorage.setItem('SUGGESTION_API_CACHE', JSON.stringify(suggestion_api_cache))
    
            return result;
        } else {
            return { bestMatches: []};
        }
    } else {
        return suggestion_api_cache[phrase];
    }
};

export const getDetailsForSymbol = async ({ phrase }) => {
    let overview_api_cache = {};
    try {
        overview_api_cache = JSON.parse(localStorage.getItem('OVERVIEW_API_CACHE')) || {};
    } catch(e) {
        console.log(e);
    }

    if (!overview_api_cache[phrase]) {
        const result =  await axiosWrapper({
            url: `query?function=${FUNCTION_TYPES.overview}&symbol=${phrase}${API_KEY}`,
            method: HTTP_METHOD.GET
        });

        let isValid = checkResponse(result);

        if (isValid) {
            overview_api_cache[phrase] = result;
            localStorage.setItem('OVERVIEW_API_CACHE', JSON.stringify(overview_api_cache));

            return result;
        } else {
            return {};
        }
    } else {
        return overview_api_cache[phrase];
    }
};